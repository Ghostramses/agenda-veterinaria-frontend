import React from 'react';

import { Link, withRouter } from 'react-router-dom';

//Importar Sweet Alert 2 (Swal)
import Swal from 'sweetalert2';

import cliente from './../config/axios';

const Cita = ({ guardarConsultar, cita, history }) => {
	if (cita === undefined) {
		history.push('/');
		return null;
	}

	//Elimina un registro
	const eliminarCita = id => {
		//Alerta de eliminado
		Swal.fire({
			title: '¿Estás seguro?',
			text: 'Una cita eliminada no se puede recuperar',
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si, Eliminar!',
			cancelButtonText: 'Cancelar'
		}).then(result => {
			if (result.value) {
				//Eliminando de la base de datos
				cliente
					.delete(`/pacientes/${id}`)
					.then(() => {
						guardarConsultar(true);
						history.push('/');
					})
					.catch(e => console.log(e));
				Swal.fire(
					'Eliminada!',
					'Su cita ha sido eliminada.',
					'success'
				);
			}
		});
	};

	return (
		<>
			<h1 className='my-5'>Nombre Cita: {cita.nombre}</h1>
			<div className='container mt-5 py-5'>
				<div className='row'>
					<div className='col-12 mb-5 d-flex justify-content-center'>
						<Link
							to={'/'}
							className='btn btn-success text-uppercase py-2 px-5 font-weight-bold'
						>
							Volver
						</Link>
					</div>
					<div className='col-md-8 mx-auto'>
						<div className='list-group'>
							<div className='p-5 list-group-item list-group-item-action flex-column align-items-center'>
								<div className='d-flex w-100 justify-content-between mb-4'>
									<h3 className='mb-3'>{cita.nombre}</h3>
									<small className='fecha-alta'>
										{cita.fecha} - {cita.hora}
									</small>
								</div>
								<p className='mb-0'>{cita.sintomas}</p>
								<div className='contacto py-3'>
									<p>Dueño: {cita.propietario}</p>
									<p>Teléfono: {cita.telefono}</p>
								</div>
								<div>
									<button
										type='button'
										className='text-uppercase py-2 px-5 font-weight-bold btn btn-danger col'
										onClick={() => eliminarCita(cita._id)}
									>
										Eliminar &times;
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default withRouter(Cita);
