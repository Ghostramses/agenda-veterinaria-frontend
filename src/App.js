import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

//Importar el cliente de axios
import clienteAxios from './config/axios';

//Componentes
import Pacientes from './components/Pacientes';
import NuevaCita from './components/NuevaCita';
import Cita from './components/Cita';

function App() {
	//Estado de la App
	const [citas, guardarCitas] = useState([]);
	const [consultar, guardarConsultar] = useState(true);

	useEffect(() => {
		if (consultar) {
			const consultarApi = async () => {
				const respuesta = await clienteAxios.get('/pacientes');
				guardarCitas(respuesta.data);
				guardarConsultar(false);
			};
			consultarApi();
		}
	}, [consultar]);

	return (
		<Router>
			<Switch>
				<Route
					exact
					path='/'
					component={() => <Pacientes citas={citas} />}
				/>
				<Route
					exact
					path='/nueva'
					component={() => (
						<NuevaCita guardarConsultar={guardarConsultar} />
					)}
				/>
				<Route
					exact
					path='/cita/:id'
					render={props => {
						let cita = citas.filter(
							cita => cita._id === props.match.params.id
						);

						return (
							<Cita
								cita={cita[0]}
								guardarConsultar={guardarConsultar}
							/>
						);
					}}
				/>
			</Switch>
		</Router>
	);
}

export default App;
